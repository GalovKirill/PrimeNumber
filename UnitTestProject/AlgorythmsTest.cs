using System;
using System.Diagnostics;
using System.Numerics;
using PrimeNumber;
using Xunit;

namespace UnitTestProject
{
    public class AlgorythmsTest
    {
        [Fact]
        public void FermaTest()
        {
            BigInteger n = BigInteger.Pow(2, 607) - 1; // prime Mersen number M607
            Stopwatch time = Stopwatch.StartNew();
            bool result = Algorythms.TestFerma(n);
            time.Stop();
            Assert.True(result, time.Elapsed.ToString());
        }

        [Fact]
        public void MillerTest()
        {
            BigInteger n = BigInteger.Pow(2, 607) - 1; // prime Mersen number M607
            Stopwatch time = Stopwatch.StartNew();
            bool result = Algorythms.MillerRabinParallelOptimize(n);
            time.Stop();
            Assert.True(result, time.Elapsed.ToString());
        }

        [Fact]
        public void SolovShtrassenTest()
        {
            BigInteger n = BigInteger.Pow(2, 607) - 1; // prime Mersen number M607
            Stopwatch time = Stopwatch.StartNew();
            bool result = Algorythms.SolovShtrassen(n);
            time.Stop();
            Assert.True(result);
        }

        [Fact]
        public void TestYacobi()
        {
            var act2 = Algorythms.Yacobi(506,1103);
            BigInteger expected = BigInteger.MinusOne;
            Assert.Equal(act2,expected);
        }
    }
}