﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace PrimeNumber
{
    public class Algorythms
    {
        private static readonly List<int> Primes = SieveOfEratosthenes(100);
        private static readonly RandomNumberGenerator Random = RandomNumberGenerator.Create();

        public static bool SolovShtrassen(BigInteger n)
        {
            int k = (int) BigInteger.Log(n, 2.0);
            bool isPrime = true;
            Parallel.For(0, k, (i, pls) =>
            {
                BigInteger randomNumber = RandomBigInteger(n);
                if (!BigInteger.GreatestCommonDivisor(n, randomNumber).IsOne)
                {
                    isPrime = false;
                    pls.Break();
                }

                var legandr = BigInteger.ModPow(randomNumber, (n - 1) / 2, n);
                var yacobi = Yacobi(randomNumber, n);
                if (yacobi == BigInteger.MinusOne)
                {
                    yacobi = n - 1;
                }
                if (legandr != yacobi)
                {
                    isPrime = false;
                    pls.Break();
                    
                }


            });
            return isPrime;
            
        }

        public static BigInteger Yacobi(BigInteger a, BigInteger b)
        {
            int r = 1;
            while (!a.IsZero)
            {
                int t = 0;
            
                while (a.IsEven)
                {
                    t++;
                    a /= 2;
                }
                
                if (t % 2 != 0)
                {
                    BigInteger temp = b % 8;
                    if (temp == 3 || temp == 5)
                    {
                        r = -r;
                    }
                }
                BigInteger a4 = a % 4, b4 = b % 4;
                if (a4 == 3 && b4 == 3)
                {
                    r = -r;
                }
                var c = a;
                a = b % c;
                b = c;
               
            }
            return r;
        }
        
        public static bool MillerRabinParallelOptimize(BigInteger n)
        {
            foreach (var p in Primes) 
                if (n % p == 0)       
                    return false;     
            int s = 0;
            BigInteger d = n - 1;
            while (d % 2 != 1)
            {
                d /= 2;
                s++;
            }
            int k = (int)BigInteger.Log(n, 2.0);           
            bool isPrime = true;
            Parallel.For(0, k, (i, pls) =>
            {
                bool roundPassed = false;
                BigInteger a = RandomBigInteger(n);
                BigInteger newa = BigInteger.ModPow(a, d, n);
                if (newa == BigInteger.One || newa == n - 1) 
                    roundPassed = true;
                if (!roundPassed) 
                    for (int r = 1; r < s; r++)
                    {
                        newa = BigInteger.ModPow(newa, 2, n);
                        if (newa == n - 1)
                        {
                            roundPassed = true;
                            break;
                        }
                    }                     
 
                if (!roundPassed)
                {
                    isPrime = false;
                    pls.Break();
                }
            });       
            return isPrime;           
        }

        public static bool TestFerma(BigInteger n)
        {
            int k = (int)BigInteger.Log(n, 2.0);
            bool isPrime = true;
            Parallel.For(0, k, (i, pls) =>
            {
                BigInteger randomNumber;
                do
                {
                    randomNumber = RandomBigInteger(n);
                } 
                while (n % randomNumber == 0);
                if (BigInteger.ModPow(randomNumber, n - 1, n) != 1)
                {
                    isPrime = false;
                    pls.Break();
                }

            });
            return isPrime;
        }
        
        private static BigInteger RandomBigInteger(BigInteger n)
        {
            byte[] bytes = new byte[(int)BigInteger.Log(n, 256.0) + 1];
            Random.GetBytes(bytes);   
            bytes[bytes.Length - 1] = 0;              
            return new BigInteger(bytes);
        }

        private static List<int> SieveOfEratosthenes(int N)
        {
            List<int> primes = new List<int>();
            var R = new BitArray(N);
            R.SetAll(true);
            int i = 0, j = 0, p = 2 * i + 3, NN = 2 * N + 3;
            while (p * p < N)
            {
                j = p * p;
                while (j < NN)
                {
                    R[(j - 3) / 2] = false;
                    j = j + 2 * p;
                }

                do
                {
                    i++;
                } while (R[i]);
                p = 2 * i + 3;
            }
            for (int r = 0; r < N; r++)
                if (R[r])
                    primes.Add(2 * r + 3);
            return primes;
        }
        
        
    }
}
